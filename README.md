# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A sample .NET Core Web API using Entity Framework with migration for Code-First approach
* With unit test project using NUnit
* Using Dependecy Injection and Repository pattern

### How do I get set up? ###

* Visual Studio 2019 16.8 or later with the ASP.NET and web development workload
* .NET 5.0 SDK
