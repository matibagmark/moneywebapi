﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoneyAPI.DL.Data;
using System;
using System.Linq;

namespace MoneyAPI.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new MoneyAPIContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<MoneyAPIContext>>()))
            {
                // Look for any movies.
                if (context.Accounts.Any())
                {
                    return;   // DB has been seeded
                }

                context.Accounts.AddRange(
                    new Account
                    {
                        AccountBalance = 50001,
                        AccountId = 100001
                    },

                    new Account
                    {
                        AccountBalance = 50002,
                        AccountId = 100002
                    },

                    new Account
                    {
                        AccountBalance = 50003,
                        AccountId = 100003
                    },


                    new Account
                    {
                        AccountBalance = 50004,
                        AccountId = 100004
                    }
                );

                context.PaymentTransactions.AddRange(
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("10/10/2021"),
                       AccountId = 100001,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = "Posted"
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("15/10/2021"),
                       AccountId = 100001,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("20/10/2021"),
                       AccountId = 100001,
                       Amount = 300,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("25/10/2021"),
                       AccountId = 100001,
                       Amount = 600,
                       Status = "Pending",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("02/10/2021"),
                       AccountId = 100002,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("07/10/2021"),
                       AccountId = 100002,
                       Amount = 600,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("06/10/2021"),
                       AccountId = 100003,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("10/10/2021"),
                       AccountId = 100003,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("08/10/2021"),
                       AccountId = 100003,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   }
                   ,
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("06/10/2021"),
                       AccountId = 100004,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("10/10/2021"),
                       AccountId = 100004,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("08/10/2021"),
                       AccountId = 100004,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   }
               );

                context.SaveChanges();
            }
        }
    }
}