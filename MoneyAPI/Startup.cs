﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MoneyAPI.BL.Services;
using MoneyAPI.DL.Data;
using MoneyAPI.DL.Repositories;
using MoneyAPI.Services;

namespace MoneyAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MoneyAPI", Version = "v1" });
            });

            services.AddDbContext<MoneyAPIContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("MoneyAPIContext")));

            services.AddScoped<IBalanceServicesAdapter, BalanceServicesAdapter>();
            services.AddScoped<IBalanceServices, BalanceServices>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IPaymentTransactionRepository, PaymentTransactionRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MoneyAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
