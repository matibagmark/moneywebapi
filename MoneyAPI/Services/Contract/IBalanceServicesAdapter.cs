﻿using MoneyAPI.BL.Models;
using System.Threading.Tasks;

namespace MoneyAPI.Services
{
    public interface IBalanceServicesAdapter
    {
        Task<Balance> GetBalance(int accountId);
    }
}