﻿using MoneyAPI.BL.Models;
using MoneyAPI.BL.Services;
using System.Threading.Tasks;

namespace MoneyAPI.Services
{
    public class BalanceServicesAdapter : IBalanceServicesAdapter
    {
        IBalanceServices _balanceServices;

        public BalanceServicesAdapter(IBalanceServices balanceServices)
        {
            _balanceServices = balanceServices;
        }

        public async Task<Balance> GetBalance(int accountId) => await _balanceServices.GetBalance(accountId);

    }
}
