﻿using Microsoft.AspNetCore.Mvc;
using MoneyAPI.Services;
using System;
using System.Threading.Tasks;

namespace MoneyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BalancesController : ControllerBase
    {
       private readonly IBalanceServicesAdapter _balanceServicesAdapter;

        public BalancesController(IBalanceServicesAdapter balanceServicesAdapter)
        {
            _balanceServicesAdapter = balanceServicesAdapter;
        }
        [HttpGet]
        [Route("Get/{accountId}")]
        public async Task<IActionResult> GetBalance(int accountId)
        {
            try
            {
                if (accountId == 0)
                {
                    return NotFound();
                }

                var result = await _balanceServicesAdapter.GetBalance(accountId);

                if (result.AccountId == 0) 
                {
                    return NotFound();
                }

                return Ok(result);

            }
            catch (Exception)
            {
                //Do logging here
                return NotFound();
            }
        }
    }
}
