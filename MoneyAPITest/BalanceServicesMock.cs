﻿using MoneyAPI.BL.Models;
using MoneyAPI.DL.Data;
using MoneyAPI.Models;
using MoneyAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoneyAPITest
{
    public class BalanceServicesMock : IBalanceServicesAdapter
    {
        public async Task<Balance> GetBalance(int accountId)
        {
            if (accountId == 10000)
            {
                return await Task.FromResult(new Balance()
                {
                    AccountId = 10000,
                    AccountBalance = 50000,
                    PaymentTransactions = new List<PaymentTransaction>()
                {
                new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("10/10/2021"),
                       AccountId = 100001,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = "Posted"
                   },
                   new PaymentTransaction
                   {
                       TransactionDate = Convert.ToDateTime("15/10/2021"),
                       AccountId = 100001,
                       Amount = 400,
                       Status = "Closed",
                       Remarks = ""
                   }
                }
                });
            }
            else 
            {
                return null;
            }
        }
    }
}
