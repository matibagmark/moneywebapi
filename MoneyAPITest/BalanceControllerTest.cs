﻿using Microsoft.AspNetCore.Mvc;
using MoneyAPI.BL.Models;
using MoneyAPI.Controllers;
using MoneyAPI.Models;
using MoneyAPI.Services;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyAPITest
{
    [TestFixture]
    public class BalanceControllerTest
    {
        private readonly BalancesController _balancesController;
        private readonly IBalanceServicesAdapter _balanceServicesAdapter;


        public BalanceControllerTest() 
        {
            _balanceServicesAdapter = new BalanceServicesMock();
            _balancesController = new BalancesController(_balanceServicesAdapter);
        }


        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public async Task Test_BalancesController_Invalid_AccountId()
        {
            int accountId = 0;

            var response = await _balancesController.GetBalance(accountId);
            var notFoundResult = response as NotFoundResult;

            // assert
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);

        }

        [Test]
        public async Task Test_BalancesController_Account_NotExisting()
        {
            int accountId = 999999;

            var response = await _balancesController.GetBalance(accountId);
            var notFoundResult = response as NotFoundResult;

            // assert
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);

        }

        [Test]
        public async Task Test_BalancesController_Account_Existing()
        {
            int accountId = 10000;

            var response = await _balancesController.GetBalance(accountId);
            var okResult = response as OkObjectResult;

            // assert
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);

        }

        [Test]
        public async Task Test_BalancesController_Account_IsValidResponse()
        {
            int accountId = 10000;

            var response = await _balancesController.GetBalance(accountId);
            var okResult = response as OkObjectResult;

            // assert
            Assert.IsNotNull(okResult);
            Assert.IsInstanceOf<Balance>(okResult.Value);

        }


        [Test]
        public async Task Test_BalancesController_Account_Existing_WithBalance()
        {
            int accountId = 10000;

            var response = await _balancesController.GetBalance(accountId);
            var okResult = response as OkObjectResult;
            var balanceResult = okResult.Value as Balance;

            // assert
            Assert.IsNotNull(okResult);
            Assert.IsNotNull(okResult.Value);
            Assert.IsTrue(balanceResult.AccountBalance > 0);
        }

        [Test]
        public async Task Test_BalancesController_Account_Existing_WithPayments()
        {
            int accountId = 10000;

            var response = await _balancesController.GetBalance(accountId);
            var okResult = response as OkObjectResult;
            var balanceResult = okResult.Value as Balance;

            // assert
            Assert.IsNotNull(okResult);
            Assert.IsNotNull(okResult.Value);
            Assert.IsTrue(balanceResult.PaymentTransactions.Any());
        }

    }
}
