﻿using MoneyAPI.BL.Models;
using System.Threading.Tasks;

namespace MoneyAPI.BL.Services
{
    public interface IBalanceServices
    {
        Task<Balance> GetBalance(int accountId);
    }
}
