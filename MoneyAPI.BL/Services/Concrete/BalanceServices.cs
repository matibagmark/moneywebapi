﻿using MoneyAPI.BL.Models;
using MoneyAPI.DL.Data;
using MoneyAPI.DL.Repositories;
using System;
using System.Threading.Tasks;

namespace MoneyAPI.BL.Services
{
    public class BalanceServices : IBalanceServices
    {
        private IAccountRepository _accountRepository;
        private IPaymentTransactionRepository _paymentTransactionRepository;

        public BalanceServices(IAccountRepository accountRepository, IPaymentTransactionRepository paymentTransactionRepository) 
        {
            _accountRepository = accountRepository;
            _paymentTransactionRepository = paymentTransactionRepository;

        }

        public async Task<Balance> GetBalance(int accountId)
        {
            Balance balanceResponse = new Balance();
            try
            {
                Account account = await _accountRepository.GetAccountById(accountId);

                if (account != null) {
                    balanceResponse.AccountId = account.AccountId;
                    balanceResponse.AccountBalance = account.AccountBalance;
                    balanceResponse.PaymentTransactions = await _paymentTransactionRepository.GetPaymentTransactionsByAccountId(accountId);
                }
            }
            catch (Exception ex)
            {
                //Do logging here
                throw;
            }
                
            return balanceResponse;
        }
    }
}
