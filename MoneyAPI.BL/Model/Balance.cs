﻿using MoneyAPI.DL.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoneyAPI.BL.Models
{
    public class Balance
    {
        [Key]
        public int AccountId { get; set; }
        public decimal AccountBalance { get; set; }
        public List<PaymentTransaction> PaymentTransactions { get; set; }
    }
}
