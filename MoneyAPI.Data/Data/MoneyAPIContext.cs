﻿using Microsoft.EntityFrameworkCore;

namespace MoneyAPI.DL.Data
{
    public class MoneyAPIContext : DbContext
    {
        public MoneyAPIContext (DbContextOptions<MoneyAPIContext> options)
            : base(options)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<PaymentTransaction> PaymentTransactions { get; set; }
    }
}
