﻿using MoneyAPI.DL.Data;
using System.Threading.Tasks;

namespace MoneyAPI.DL.Repositories
{
    public interface IAccountRepository
    {
        Task<Account> GetAccountById(int accountId);
    }
}
