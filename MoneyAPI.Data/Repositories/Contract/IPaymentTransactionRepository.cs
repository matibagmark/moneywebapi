﻿using MoneyAPI.DL.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoneyAPI.DL.Repositories
{
    public interface IPaymentTransactionRepository
    {
        Task<List<PaymentTransaction>> GetPaymentTransactionsByAccountId(int accountId);
    }
}
