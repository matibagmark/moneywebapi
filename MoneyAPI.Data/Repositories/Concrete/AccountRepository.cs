﻿using Microsoft.EntityFrameworkCore;
using MoneyAPI.DL.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyAPI.DL.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly MoneyAPIContext _moneyAPIContext;

        public AccountRepository(MoneyAPIContext moneyAPIContext) 
        {
            _moneyAPIContext = moneyAPIContext;
        }
        public async Task<Account> GetAccountById(int accountId)
        {
            try
            {
               return await _moneyAPIContext.Accounts.Where(x => x.AccountId == accountId).SingleOrDefaultAsync();
            }
            catch (Exception)
            {
                //Do logging here
                throw;
            }
           
        }
    }
}
