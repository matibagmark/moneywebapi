﻿using Microsoft.EntityFrameworkCore;
using MoneyAPI.DL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyAPI.DL.Repositories
{
    public class PaymentTransactionRepository : IPaymentTransactionRepository
    {
        private readonly MoneyAPIContext _moneyAPIContext;
        public PaymentTransactionRepository(MoneyAPIContext moneyAPIContext)
        {
            _moneyAPIContext = moneyAPIContext;
        }
        public async Task<List<PaymentTransaction>> GetPaymentTransactionsByAccountId(int accountId)
        {
            List<PaymentTransaction> paymentTransactions = new List<PaymentTransaction>();
            try
            {
                paymentTransactions = await _moneyAPIContext.PaymentTransactions.Where(x => x.AccountId == accountId).OrderByDescending(y => y.TransactionDate).ToListAsync();
            }
            catch (Exception)
            {
                //Do logging here
                throw;
            }

            return paymentTransactions;
        }
    }
}
